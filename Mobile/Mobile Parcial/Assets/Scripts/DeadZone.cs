using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZone : MonoBehaviour
{
    private bool deadly = false;

    [SerializeField] private bool DeathOnTouchMode;

    [SerializeField] private Color DeathColor;
    [SerializeField] private Color SafeColor;

    [SerializeField] private GameObject spriteDeath;
    void Update()
    {
        if (DeathOnTouchMode)
        {
            if (Input.touchCount <= 0)
            {
                GetComponent<SpriteRenderer>().color = SafeColor;
                deadly = false;
                spriteDeath.SetActive(false);
            }
            else
            {
                GetComponent<SpriteRenderer>().color = DeathColor;
                deadly = true;
                spriteDeath.SetActive(true);
            }
        }
        else
        {
            if (Input.touchCount > 0)
            {
                GetComponent<SpriteRenderer>().color = SafeColor;
                deadly = false;
                spriteDeath.SetActive(false);
            }
            else
            {
                GetComponent<SpriteRenderer>().color = DeathColor;
                deadly = true;
                spriteDeath.SetActive(true);
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (deadly && collision.CompareTag("Player"))
        {
            GameManager.GameOver();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (deadly && collision.CompareTag("Player"))
        {
            GameManager.GameOver();
        }
    }
}
