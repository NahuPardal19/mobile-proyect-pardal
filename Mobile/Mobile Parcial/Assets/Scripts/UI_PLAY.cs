using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_PLAY : MonoBehaviour
{

    public void Restart()
    {
        print("restart");
        GameManager.GameOver();
    }

    public void GoToMenu()
    {
        print("menu");
        GameManager.PassLevel(0);
    }
}
