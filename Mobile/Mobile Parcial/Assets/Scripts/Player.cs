using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody2D rb;
    float dirx;
    [SerializeField] private float speed;
    [SerializeField] private float decreaseRate = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Move();
    }
    private void FixedUpdate()
    {
        if (Input.touchCount <= 0)
        {
            rb.velocity = new Vector2(dirx, rb.velocity.y);
            rb.isKinematic = false;
        }
        else
        {
            float newVelocityX = Mathf.Lerp(rb.velocity.x, 0, decreaseRate * Time.deltaTime);
            rb.velocity = new Vector2(newVelocityX, rb.velocity.y);
        }
    }

    private void Move()
    {
        dirx = Input.acceleration.x * speed;
    }


}
