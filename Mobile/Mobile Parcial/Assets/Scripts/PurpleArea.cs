using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurpleArea : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))  GameManager.IsOnYellowMode = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) GameManager.IsOnYellowMode = false;
    }

    private void Update()
    {
        print(GameManager.IsOnYellowMode);
    }
}
