using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flag : MonoBehaviour
{
    [SerializeField] private int LevelIndex;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            GameManager.PassLevel(LevelIndex);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            GameManager.PassLevel(LevelIndex);
        }
    }
}
