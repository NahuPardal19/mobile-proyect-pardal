using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static bool IsOnYellowMode = false;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public static void GameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public static void PassLevel(int index)
    {
        SceneManager.LoadScene(index);
    }
}
