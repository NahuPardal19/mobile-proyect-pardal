using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotTrap : MonoBehaviour
{
    [SerializeField] private Transform transformSpawn;

    [SerializeField] private GameObject bullet;
    [SerializeField] private int PoolSize = 10;
    [SerializeField] private List<GameObject> PoolList;

    private float timeCount;
    [SerializeField] private float timePerBullet;
    private enum directionEnum {Up,Down,Left,Right}
    [SerializeField] private directionEnum direction;
    private Vector2 directionVector;

    [SerializeField] private float bulletForce;

    void Start()
    {
        AddBulletToPool(PoolSize);  

        if(direction == directionEnum.Up)
        {
            directionVector = Vector2.up;
        }
        else if (direction == directionEnum.Down)
        {
            directionVector = Vector2.down;
        }
        else if (direction == directionEnum.Left)
        {
            directionVector = Vector2.left;
        }
        else if (direction == directionEnum.Right)
        {
            directionVector = Vector2.right;
        }
    }

    void Update()
    {
        if (timeCount < timePerBullet)
        {
            timeCount += Time.deltaTime;
        }
        else
        {
            Shot();
        }
    }

    private void Shot()
    {
        GameObject bulletshot = GiveBullet();
        bulletshot.transform.position = transformSpawn.position;
        bulletshot.GetComponent<Rigidbody2D>().AddForce(directionVector * bulletForce, ForceMode2D.Impulse);
        timeCount = 0;
    }
    private void AddBulletToPool(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject bulletInstance = Instantiate(bullet, transformSpawn.position, transform.rotation, transform);
            bulletInstance.SetActive(false);
            PoolList.Add(bulletInstance);
        }
    }
    private GameObject GiveBullet()
    {
        for (int i = 0; i < PoolList.Count; i++)
        {
            if (!PoolList[i].activeSelf)
            {
                PoolList[i].SetActive(true);
                return PoolList[i];
            }
        }

        AddBulletToPool(1);
        PoolList[PoolList.Count - 1].SetActive(true);
        return PoolList[PoolList.Count - 1];
    }
}
