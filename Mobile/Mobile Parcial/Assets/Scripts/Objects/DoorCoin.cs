using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCoin : MonoBehaviour
{
    private GameObject[] Doors;
    void Start()
    {
        Doors = GameObject.FindGameObjectsWithTag("Door");
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            foreach (GameObject door in Doors)
            {
                Destroy(door);
            }
            Destroy(gameObject);
        }
    }
}
