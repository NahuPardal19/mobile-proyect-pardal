using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxMove : MonoBehaviour
{
    private Rigidbody2D rb;
    private float dirx;
    private float diry;

    [SerializeField] private float speed;

    [SerializeField] private bool IsYellow;

    GameObject player;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        MoveBox();
    }
    private void FixedUpdate()
    {
            if (Input.touchCount > 0 && (IsYellow && GameManager.IsOnYellowMode || !IsYellow && !GameManager.IsOnYellowMode))
            {
                rb.velocity = new Vector2(dirx, diry);
                rb.isKinematic = false;
                
            }
            else
            {
                rb.velocity = Vector2.zero;
                rb.isKinematic = true;
                player = null; 
            }
    }
    private void MoveBox()
    {
        dirx = Input.acceleration.x * speed;
        diry = Input.acceleration.y * speed;

    }


}
