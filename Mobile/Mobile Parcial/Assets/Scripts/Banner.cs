using System;
using UnityEngine;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using Unity.VisualScripting;

public class Banner : MonoBehaviour
{
    private BannerView bannerView;

    public void Start()
    {
        MobileAds.Initialize(status => { });
        this.RequestBanner();
    }

    private void RequestBanner()
    {
        string adUnitId = "ca-app-pub-9844220965370804~9742733159";

        this.bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
        
        // create our request used to load the ad.
        var adRequest = new AdRequest();

        // send the request to load the ad.
        Debug.Log("Loading banner ad.");
        this.bannerView.LoadAd(adRequest);
    }
}