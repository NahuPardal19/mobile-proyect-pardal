using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    [SerializeField] private Color colorNormal;
    [SerializeField] private Color colorInTouch;
    [SerializeField] private Color PurpleColorInTouch;

    void Start()
    {
        
    }

    void Update()
    {
        if (Input.touchCount <= 0)
        {
            GetComponent<SpriteRenderer>().color = colorNormal;
        }
        else
        {
            if (GameManager.IsOnYellowMode)
            {
                GetComponent<SpriteRenderer>().color = PurpleColorInTouch;
            }
            else GetComponent<SpriteRenderer>().color = colorInTouch;
        }
    }
}
